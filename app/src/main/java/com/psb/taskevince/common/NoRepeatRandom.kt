package com.psb.taskevince.common

public class NoRepeatRandom(minVal: Int, maxVal: Int) {
    private var number: IntArray? = null
    private var n = -1
    private var size = 0

    init {
        n = maxVal - minVal + 1
        number = IntArray(n)
        var n = minVal
        for (i in 0 until this.n) number!![i] = n++
        size = this.n
    }

    fun reset() {
        size = n
    }

    fun getRandom(): Int {
        if (size <= 0) return -1
        val index = (size * Math.random()).toInt()
        val randNum = number!![index]

        number!![index] = number!![size - 1]
        number!![--size] = randNum
        return randNum
    }
}

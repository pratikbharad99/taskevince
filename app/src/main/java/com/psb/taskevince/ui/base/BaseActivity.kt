package com.psb.taskevince.ui.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    val activity by lazy { this }

}
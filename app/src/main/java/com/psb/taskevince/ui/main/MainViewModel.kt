package com.psb.taskevince.ui.main

import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatSpinner
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.psb.taskevince.R
import com.psb.taskevince.common.NoRepeatRandom
import com.psb.taskevince.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class MainViewModel : BaseViewModel() {

    val selectedItem = MutableLiveData<Int>()
    val isBtnEnable = MutableLiveData<Boolean>()

    private val dropDownArray by lazy { arrayListOf(2, 3, 4, 5, 6, 7, 8, 9) }
    private lateinit var noRepeatRandom: NoRepeatRandom

    fun setupSpinnerAdapter(spinnerMain: AppCompatSpinner) {
        uiScope.launch {
            val adapter: ArrayAdapter<Int> = ArrayAdapter<Int>(
                spinnerMain.context,
                android.R.layout.simple_spinner_dropdown_item,
                dropDownArray
            )
            spinnerMain.adapter = adapter
            spinnerMain.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    selectedItem.value = dropDownArray[p2]
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
        }
    }

    fun setUpGrid(
        number: Int,
        layoutConstraint: ConstraintLayout,
        viewGrid: View
    ) {
        uiScope.launch {
            val count = layoutConstraint.childCount - 1
            for (i in count downTo 1) {
                layoutConstraint.removeViewAt(i)
            }
            layoutConstraint.invalidate()
            isBtnEnable.value = true
            noRepeatRandom = NoRepeatRandom(1, number * number)
            var textNumber: TextView
            var layoutParams: ConstraintLayout.LayoutParams
            var id: Int
            val idArray = Array(number) {
                IntArray(
                    number
                )
            }
            val constraintSet = ConstraintSet()
            var idCounter = 1

            for (iRow in 0 until number) {
                for (iCol in 0 until number) {
                    textNumber = TextView(layoutConstraint.context)
                    layoutParams = ConstraintLayout.LayoutParams(
                        ConstraintSet.MATCH_CONSTRAINT,
                        ConstraintSet.MATCH_CONSTRAINT
                    )
                    id = idCounter++
                    Log.d("id", id.toString())
                    idArray[iRow][iCol] = id
                    textNumber.id = id
                    textNumber.text = id.toString()
                    textNumber.gravity = Gravity.CENTER
                    textNumber.isEnabled = false
                    textNumber.isClickable = false
                    textNumber.setTextColor(
                        ContextCompat.getColor(
                            layoutConstraint.context,
                            R.color.black
                        )
                    )
                    textNumber.background = ContextCompat.getDrawable(
                        layoutConstraint.context,
                        R.drawable.bg_griditem
                    )
                    layoutConstraint.addView(textNumber, layoutParams)
                }
            }

            constraintSet.clone(layoutConstraint)
            constraintSet.setDimensionRatio(viewGrid.id, "$number:$number")
            for (iRow in 0 until number) {
                for (iCol in 0 until number) {
                    id = idArray[iRow][iCol]
                    constraintSet.setDimensionRatio(id, "1:1")
                    if (iRow == 0) {
                        constraintSet.connect(id, ConstraintSet.TOP, viewGrid.id, ConstraintSet.TOP)
                    } else {
                        constraintSet.connect(
                            id,
                            ConstraintSet.TOP,
                            idArray[iRow - 1][0],
                            ConstraintSet.BOTTOM
                        )
                    }
                }
                constraintSet.createHorizontalChain(
                    viewGrid.id, ConstraintSet.LEFT,
                    viewGrid.id, ConstraintSet.RIGHT,
                    idArray[iRow], null, ConstraintSet.CHAIN_PACKED
                )
            }

            constraintSet.applyTo(layoutConstraint)
        }
    }

    fun applyButtonClick(
        layoutConstraint: ConstraintLayout
    ) {
        val random: Int = noRepeatRandom.getRandom()
        if (random == -1) {
            Toast.makeText(layoutConstraint.context, "Please generate new grid", Toast.LENGTH_LONG)
                .show()
        } else {
            isBtnEnable.value = false
            layoutConstraint.getViewById(random)?.let {
                it.isEnabled = true
                it.isClickable = true
                it.setOnClickListener {
                    isBtnEnable.value = true
                    layoutConstraint.getViewById(random).isClickable = false
                }
            }
        }
    }
}
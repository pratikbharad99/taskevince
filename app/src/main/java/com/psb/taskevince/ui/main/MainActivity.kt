package com.psb.taskevince.ui.main

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.psb.taskevince.databinding.ActivityMainBinding
import com.psb.taskevince.ui.base.BaseActivity

class MainActivity : BaseActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel by lazy { ViewModelProvider(this)[MainViewModel::class.java] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setUpDropDown()
    }

    private fun setUpDropDown() {
        viewModel.selectedItem.observe(this) {
            viewModel.setUpGrid(it, binding.layoutConstraint, binding.viewGrid)
        }

        viewModel.isBtnEnable.observe(this) {
            binding.btnRandomNumberGenerator.isEnabled = it
        }

        viewModel.setupSpinnerAdapter(binding.spinnerMain)

        binding.btnRandomNumberGenerator.setOnClickListener {
            viewModel.applyButtonClick(binding.layoutConstraint)
        }
    }

}